export CONFD_IPC_PORT=5014
export NETCONF_SSH_PORT=12026
export NETCONF_TCP_PORT=13026
export CLI_SSH_PORT=10026
export SNMP_PORT=11026
export NAME=xr1
export COUNTER=4
export TWO_DIGIT_COUNTER=04
export CONFD=${NCS_DIR}/netsim/confd/bin/confd
export CONFDC=${NCS_DIR}/netsim/confd/bin/confdc
export CONFD_DIR=${NCS_DIR}/netsim/confd
export PATH=${NCS_DIR}/netsim/confd/bin:/Users/pfinnega/tail-f/4.7/bin:/Library/Frameworks/Python.framework/Versions/3.7/bin:/usr/local/opt/openssl/bin:/opt/local/Library/Frameworks/Python.framework/Versions/2.7/bin:/opt/local/bin:/opt/local/sbin:/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin:/opt/local/bin:/Users/pfinnega/tail-f/script:/opt/local/sbin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/opt/local/bin:/opt/X11/bin:/usr/local/bin/jamf
export PACKAGE_NETSIM_DIR=../../../../4.7/packages/neds/cisco-iosxr/netsim
